var Knex = require('knex');
var Bookshelf = require('bookshelf');
var knexConfig = require('../knexfile');
var knex = Knex(knexConfig[process.env.NODE_ENV === 'production' ? 'production' : 'development']);
var db = Bookshelf(knex);

db.plugin(['registry', 'virtuals', 'visibility']);

require('./models')(db);

module.exports = db;