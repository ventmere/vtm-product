var Promise = require('bluebird'),
	_ = require('lodash');

module.exports = function (Db) {
	var prototype = Db.Model.prototype;

	var SKU = Db.Model.extend({
		tableName: 'sku',
		hasTimestamps: true
	});

	Db.model('SKU', SKU);
};