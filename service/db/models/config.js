var Promise = require('bluebird'),
	_ = require('lodash');

module.exports = function (Db) {
	var prototype = Db.Model.prototype;

	var Config = Db.Model.extend({
		tableName: 'config'
	}, {
		get: function(key) {
			return Config.where('key', key).fetch().then(function(model) {
				var value = model ? model.get('value') : null
				if (value) {
					try {
						value = JSON.parse(value);
					} catch (_) {}
				}
				return value;
			});
		},
		set: function(key, value) {
			return Config.where('key', key).fetch().then(function(model) {
				if (!model) {
					model = Config.forge({
						key: key
					});
				}
				if (value) {
					try {
						value = JSON.stringify(value);
					} catch (_) {
						value = null;
					}
					model.set('value', value);
					return model.save();
				} else {
					if (model.id) {
						return model.destroy();
					}
				}
			});
		}
	});

	Db.model('Config', Config);
};