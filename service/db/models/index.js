var fs = require('fs'),
    _ = require('lodash');

module.exports = function (Db) {
    _.each(fs.readdirSync(__dirname), function (file) {
        if (file != 'index.js') {
        	var matches = file.match(/^([\w-]+)\.js$/);
        	if (matches) {
            	var moduleName = matches[1];
            	require('./' + moduleName)(Db);
        	}
        }
    });
};