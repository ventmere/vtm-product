var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var routes = require('./routes');

var app = express();

app.use(express.static(__dirname + '/public'));

app.use(cors());
app.use(bodyParser.json());

routes(app);

app.listen(3000, function() {
	console.log('Ventmere Product Service started.');
});