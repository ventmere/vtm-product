'use strict';

exports.up = function(knex, Promise) {
	return knex.schema
		.createTable('config', function(table) {
			table.string('key').primary();
			table.string('value');
		})
		.createTable('currency', function(table) {
			table.string('id', 3).primary();
			table.string('name');
		})
		.createTable('country', function(table) {
			table.string('id', 2).primary();
			table.string('name');
		})
		.createTable('sku', function(table) {
			table.string('id').primary();
			table.string('name').index();
			table.text('description');
			table.timestamps();
		})
		.createTable('skuprice', function(table) {
			table.increments('id').primary();
			table.string('sku_id').references('id').inTable('sku').onDelete('CASCADE');
			table.string('name');
			table.string('currency_id', 3).references('id').inTable('currency').onDelete('CASCADE');
			table.decimal('value', 19, 4).defaultTo(0);
			table.timestamps();
		})
		.createTable('skumeta', function(table) {
			table.increments('id').primary();
			table.string('sku_id').references('id').inTable('sku').onDelete('CASCADE');
			table.string('key').index();
			table.text('value');
			table.timestamps();
		})
		.createTable('address', function(table) {
			table.increments('id').primary();
			table.string('name');
			table.string('phone');
			table.string('address');
			table.string('city');
			table.string('region');
			table.string('country_id').references('id').inTable('country');
			table.string('postal_code');
			table.timestamps();
		})
		.createTable('customer', function(table) {
			table.increments('id').primary();
			table.string('email');
			table.string('password');
			table.string('name');
			table.timestamps();
		})
		.createTable('order', function(table) {
			table.string('id').primary();
			table.integer('customer_id').unsigned().references('id').inTable('customer').onDelete('SET NULL');
			table.string('currency_id').references('id').inTable('currency').onDelete('SET NULL');
			table.decimal('total', 19, 4);
			table.timestamps();
		})
		.createTable('orderitem', function(table) {
			table.increments('id').primary();
			//table.string('order_id').references('id').inTable('order').onDelete('CASCADE');
			table.string('name');
			table.text('description');
			table.decimal('unit_price', 19, 4);
			table.integer('quantity').defaultTo(1);
			table.timestamps();
		})
		.createTable('skureview', function(table) {
			table.increments('id').primary();
			table.string('sku_id').references('id').inTable('sku').onDelete('CASCADE');
			//table.string('order_id').references('id').inTable('order').onDelete('SET NULL');
			table.integer('customer_id').unsigned().references('id').inTable('customer').onDelete('SET NULL');
			table.integer('rating');
			table.string('title');
			table.text('content');
			table.timestamps();
		})
		.createTable('cart', function(table) {
			table.string('id').primary();
		})
		.createTable('cartitem', function(table) {
			table.increments('id').primary();
			table.string('cart_id').references('id').inTable('sku').onDelete('CASCADE');
			table.string('sku_id').references('id').inTable('sku').onDelete('CASCADE');
			table.integer('quantity');
			table.timestamps();
		});
};

exports.down = function(knex, Promise) {
	var tables = [
		'currency',
		'country',
		'sku',
		'skuprice',
		'skumeta',
		'address',
		'customer',
		'order',
		'orderitem',
		'skureview',
		'cart',
		'cartitem'
	];
	return Promise.map(tables.reverse(), function(tableName) {
		return knex.schema.dropTableIfExists(tableName);
	});
};
