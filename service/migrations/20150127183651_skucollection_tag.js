'use strict';

exports.up = function(knex, Promise) {
	return knex.transaction(function(trx) {
		return trx.schema
			.createTable('skucollection', function(table) {
				table.increments('id').primary();
				table.string('name').index();
				table.boolean('visible').defaultTo(true);
				table.timestamps();
			})
			.createTable('sku_skucollection', function(table) {
				table.increments('id').primary();
				table.string('sku_id').references('id').inTable('sku').onDelete('CASCADE');
				table.integer('skucollection_id').unsigned().references('id').inTable('skucollection').onDelete('CASCADE');

				table.index(['sku_id', 'skucollection_id']);
			})
			.createTable('tag', function(table) {
				table.string('id').primary();
			});
	});
};

exports.down = function(knex, Promise) {
	return knex.transaction(function(trx) {
		return trx.schema
			.dropTableIfExists('skucollection')
			.dropTableIfExists('sku_skucollection')
			.dropTableIfExists('tag');
	});
};
