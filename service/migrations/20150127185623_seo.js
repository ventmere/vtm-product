'use strict';

exports.up = function(knex, Promise) {
	return knex.transaction(function(trx) {
		return trx.schema
			.table('sku', function(table) {
				table.string('seo_title');
				table.string('seo_description');
			})
			.table('skucollection', function(table) {
				table.string('seo_title');
				table.string('seo_description');
			});
	});
};

exports.down = function(knex, Promise) {
	return knex.transaction(function(trx) {
		return trx.schema
			.table('sku', function(table) {
				table.dropColumns('seo_title', 'seo_description');
			})
			.table('skucollection', function(table) {
				table.dropColumns('seo_title', 'seo_description');
			});
	});
};
