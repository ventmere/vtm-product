'use strict';

exports.up = function(knex, Promise) {
	return knex.transaction(function(trx) {
		return trx.schema
			.createTable('media', function(table) {
				table.increments('id').primary();
				table.string('url');
				table.string('title');
				table.text('meta');
				table.timestamps();
			})
			.createTable('sku_media', function(table) {
				table.increments('id').primary();
				table.string('sku_id').references('id').inTable('sku').onDelete('CASCADE');
				table.integer('media_id').unsigned().references('id').inTable('media').onDelete('CASCADE');
				table.integer('order').defaultTo(0);
				table.integer('role');
				table.timestamps();
			})
			.createTable('skucollection_media', function(table) {
				table.increments('id').primary();
				table.integer('skucollection_id').unsigned().references('id').inTable('skucollection').onDelete('CASCADE');
				table.integer('media_id').unsigned().references('id').inTable('media').onDelete('CASCADE');
				table.integer('order').defaultTo(0);
				table.integer('role');
				table.timestamps();
			})
	});
};

exports.down = function(knex, Promise) {
	return knex.transaction(function(trx) {
		return trx.schema
			.dropTableIfExists('skucollection_media')
			.dropTableIfExists('sku_media')
			.dropTableIfExists('media');
	});
};
