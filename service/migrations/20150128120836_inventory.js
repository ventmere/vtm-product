'use strict';

exports.up = function(knex, Promise) {
	return knex.transaction(function(trx) {
		return trx.schema
			.table('sku', function(table) {
				table.boolean('track_inventory');
				table.integer('inventory');
			})
	});
};

exports.down = function(knex, Promise) {
	return knex.transaction(function(trx) {
		return trx.schema
			.table('sku', function(table) {
				table.dropColumn('track_inventory');
				table.dropColumn('inventory');
			})
	});
};
