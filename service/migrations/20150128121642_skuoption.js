'use strict';

exports.up = function(knex, Promise) {
	return knex.schema.createTable('skuoption', function(table) {
		table.increments('id').primary();
		table.string('sku_id').references('id').inTable('sku').onDelete('CASCADE');
		table.string('name');
		table.string('detault_value');
		table.text('data');
	})
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTableIfExists('skuoption');
};
