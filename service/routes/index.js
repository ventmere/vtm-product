module.exports = function(app) {
	
	var sku = require('./sku');
	app.route('/sku')
		.get(sku.list)
		.post(sku.create);

	app.route('/sku/:id')
		.get(sku.get)
		.put(sku.update)
		.delete(sku.delete);

};