var _ = require('lodash');
var db = require('../db');
var querylist = require('../lib/querylist');
var SKU = db.model('SKU');

var fetch = function(id) {
	return SKU.where('id', id).fetch();
}

// GET
exports.list = function(req, res, next) {
	SKU.query(querylist.query(req.params)).fetchAll()
	.then(function(models) {
		res.send(models);
	})
	.catch(next);
};

// GET :id
exports.get = function(req, res, next) {
	var id = req.params.id;
	fetch(id).then(function(model) {
		if (!model) {
			return res.status(404).send({
				message: 'SKU not found'
			});
		}

		res.send(model);
	});
};

// POST
exports.create = function(req, res, next) {
	var body = _.pick(req.body, [
		'name',
		'description',
		'prices',
		'meta',
		
	]);

};

// DELETE :id
exports.delete = function(req, res, next) {
	
};

// PUT :id
exports.update = function(req, res, next) {
	
};