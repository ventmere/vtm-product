var bundler = require('vtm-bundler'),
	gulp = bundler.gulp,
	grename = require('gulp-rename'),
	gclean = require('gulp-clean'),
	guglify = require('gulp-uglify'),
	gmincss = require('gulp-minify-css');

var ui = bundler.bundle('./src');

var buildTasks = ui.buildTasks().concat('copy-assets');
var watchTasks = ui.watchTasks();

gulp.task('build', buildTasks);
gulp.task('watch', watchTasks);

gulp.task('minify-scripts', ['build'], function() {
	return gulp.src('./build/*.js')
		.pipe(guglify())
		.pipe(grename(function(path) {
			path.extname = '.min' + path.extname;
		}))
		.pipe(gulp.dest('./dist/build'));
});

gulp.task('minify-styles', ['build'], function() {
	return gulp.src('./build/*.css')
		.pipe(gmincss())
		.pipe(grename(function(path) {
			path.extname = '.min' + path.extname;
		}))
		.pipe(gulp.dest('./dist/build'));
});

gulp.task('copy-assets', function() {
	gulp.src('src/assets/**/*')
		.pipe(gulp.dest('build/assets'));
	gulp.src('src/assets/**/*')
		.pipe(gulp.dest('dist/build/assets'));
});

gulp.task('copy-built-assets', ['build'], function() {
	gulp.src('build/assets/**/*')
		.pipe(gulp.dest('dist/build/assets'));
});

gulp.task('clean', function() {
	return gulp.src(['./build/*', './dist/build/*'], {read: false})
        .pipe(gclean());
});

gulp.task('dist', ['minify-scripts', 'minify-styles', 'copy-built-assets']);