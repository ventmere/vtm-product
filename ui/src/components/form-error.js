/** @jsx m */
var m = require('mithril');
var _ = require('lodash');

exports.controller = function(props) {
	this.props = props;
};

var renderError = function(error) {
	if (error.validation_errors) {
		return (
			<ul class="list-unstyled">
				{_.map(error.validation_errors, function(item, key) {
					return (
						<li>
							{item.map(function(item) {
								return <div>{item}</div>;
							})}
						</li>);
				})}
			</ul>
		);
	} else {
		return error.message || 'Unknown error';
	}
};

exports.view = function(ctrl) {
	var alert = null;
	var error = ctrl.props();
	if (error) {
		alert = (
			<div class="alert alert-danger">
				{renderError(error)}
			</div>
		);
	}
	return alert;
};