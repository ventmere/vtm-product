/** @jsx m */
var m = require('mithril');
var _ = require('lodash');

exports.controller = function(props) {
	this.props = props;
	this.model = props.model;
	this.newVar = {
		name: m.prop(''),
		value: m.prop('')
	};

	var elem;
	var keyElem;
	var valueElem;

	this.handleAdd = function() {
		var name = this.newVar.name();
		var value = this.newVar.value();

		if (!(name && value)) {
			return;
		}

		var pairs = this.model();

		var existingIndex = _.findIndex(pairs, function(item) {
			return item.name === name;
		});
		if (existingIndex !== -1) {
			var item = pairs.splice(existingIndex, 1);
			console.log(item);
			item[0].value = value;
			pairs.push(item[0]);
		} else {
			pairs.push({
				name: name,
				value: value
			});
		}

		this.newVar.name('');
		this.newVar.value('');

		keyElem.focus();
	}.bind(this);

	this.handleRemove = function(index) {
		this.model().splice(index, 1);
	}.bind(this);

	this.handleChange = function(index, value) {
		this.model()[index].value = value;	
	};

	this.handleNameKeyDown = function(e) {
		var value = e.target.value;
		this.newVar.name(value);
		if (e.keyCode === 13) {
			e.preventDefault();
			if (value) {
				valueElem.focus();
			}
		}
	}.bind(this);

	this.handleValueKeyDown = function(e) {
		var value = e.target.value;
		this.newVar.value(value);
		if (e.keyCode === 13) {
			e.preventDefault();
			this.handleAdd();
		}
	}.bind(this);

	this.config = function(element, isInited, context) {
		elem = element;
		keyElem = element.querySelector('input[ref="key"]');
		valueElem = element.querySelector('input[ref="value"]');
	}.bind(this);
};

exports.view = function(ctrl) {
	var list = ctrl.model().map(function(item, index) {
		var buttons;
		if (!ctrl.props.fixedKeys) {
			buttons = <button class="btn btn-default" type="button" onclick={ctrl.handleRemove.bind(index)}>Remove</button>;
		}
		return (
			<tr key={index}>
				<td>{item.name}</td>
				<td>
					<input type="text" value={item.value} class="form-control" onchange={m.withAttr('value', ctrl.handleChange.bind(ctrl, index))} />
				</td>
				<td>
					{buttons}
				</td>
			</tr>
		);
	});
	return (
		<table class="table table-hover table-bordered propeditor" config={ctrl.config}>
			<thead>
				<tr>
					<th>Name</th>
					<th>Value</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				{list}
				<tr key="add">
					<td width="200px">
						<input class="form-control" ref="key" placeholder="Name" 
							value={ctrl.newVar.name()} 
							onkeydown={ctrl.handleNameKeyDown} />
					</td>
					<td>
						<input class="form-control" ref="value" placeholder="Value" 
							value={ctrl.newVar.value()} 
							onkeydown={ctrl.handleValueKeyDown} />
					</td>
					<td>
						<button class="btn btn-default" type="button" onclick={ctrl.handleAdd}>Add</button>
					</td>
				</tr>
			</tbody>
		</table>
	);
};