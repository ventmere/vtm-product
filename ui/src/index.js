/** @jsx m */
var m = require('mithril');
var request = require('./lib/request');

window.Ventmere = window.Ventmere || {};
window.Ventmere.ShoppingCart = function(opts) {
    opts = opts || {};

    if (opts.serviceBaseUrl) {
        request.setBaseUrl(opts.serviceBaseUrl);
    }

    var container = opts.container || document.body;
    var app = document.createElement('div');
    container.appendChild(app);

    var loader = document.createElement('div');
    loader.className = 'app-loader';
    app.appendChild(loader);

    m.route(app, "/shoppingcart/dashboard", {
        "/shoppingcart/dashboard": require('./modules/dashboard'),
        "/shoppingcart/sku": require('./modules/sku/list'),
        "/shoppingcart/sku-create": require('./modules/sku/create')
    });
};