/** @jsx m */
var m = require('mithril');

var loader = require('./lib/loader');
var toastr = require('toastr');
var dispatcher = require('./dispatcher');

dispatcher.on('errorMessage', function(message) {
	toastr.error(message);
});

dispatcher.on('successMessage', function(message) {
	toastr.success(message);
});

module.exports = function(content, props) {
	var checkActive = function(uri) {
		return m.route().match(/^[^\/]*\/(\w+)/)[1] === uri ? 'active' : '';
	};

	props = props || {};

	return (
		<div>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">Shopping Card Module</a>
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<li class={checkActive('dashboard')}><a href="/shoppingcart/dashboard" config={m.route}>Dashboard</a></li>
							<li class={checkActive('sku')}><a href="/shoppingcart/sku" config={m.route}>SKU</a></li>
							<li class={checkActive('order')}><a href="/shoppingcart/order" config={m.route}>Orders</a></li>
							<li class={checkActive('customer')}><a href="/shoppingcart/customer" config={m.route}>Customers</a></li>
							<li class={checkActive('setting')}><a href="/shoppingcart/setting" config={m.route}>Settings</a></li>
						</ul>
					</div>
				</div>
			</nav>
			<div class="container">
				{content}
			</div>
		</div>
	);
};