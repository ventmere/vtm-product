var moment = require('moment');

exports.date = function(str) {
	return moment(str).format('MMMM Do YYYY, h:mm:ss a');
};