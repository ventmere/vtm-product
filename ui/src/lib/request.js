/** @jsx m */
var m = require('mithril');

var cache = {};
var loaderElem;
var baseUrl = '';

var request = function(args) {
	if (!loaderElem) {
		loaderElem = document.createElement('div');
		loaderElem.className = 'request-loader';
	}
	if (args.url) {
		args.url = request.getUrl(args.url);
	}
	var key = JSON.stringify(args)
	if (!cache[key]) {

	    //show loader
	    document.body.appendChild(loaderElem);

	    var expire = function(data) {
	        delete cache[key];

	        if (Object.keys(cache).length == 0) {
	            //hide loader
	    		document.body.removeChild(loaderElem);
	        }

	        return data;
	    }

	    cache[key] = m.request(args).then(expire, function(error) {
	        expire(error);
	        throw error;
	    })
	}
	return cache[key];
};

request.setBaseUrl = function(url) {
	baseUrl = url;
};

request.getUrl = function(path) {
	return baseUrl + path;	
};

module.exports = request;