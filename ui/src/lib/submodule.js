module.exports = function(module, args) {
	return module.view.bind(this, new module.controller(args));
};