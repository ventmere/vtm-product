/** @jsx m */
var m = require('mithril');
var _ = require('lodash');
var toastr = require('toastr');

var layout = require('../../layout');
var submodule = require('../../lib/submodule');
var filters = require('../../lib/filters');
var request = require('../../lib/request');
var FormErrors = require('../../components/form-error');

var model = require('./model');

var getViewModel = function() {
	return {
		instance_id: m.prop(null),
		name: m.prop(''),
		image: m.prop(null),
		volumns: m.prop([])
	};
};

module.exports = {
	controller: function() {
		this.error = m.prop(null);
		this.images = m.prop([]);
		this.volumnsText = m.prop('');
		this.vm = getViewModel();
		this.formErrors = submodule(FormErrors, this.error);
		this.handleSubmit = function(e) {
			e.preventDefault();
			this.error(null);
			model.create(this.vm).then(function(data) {
				toastr.success('Data container created.');
				m.route('/datacontainer/' + data.id);
			}, this.error);
		}.bind(this);

		this.handleVolumnsTextChange = function(e) {
			var value = e.target.value || '';
			this.volumnsText(value);
			var rows = _.compact(value.replace(/[\r\n]+/g, '\n').split('\n'));
			this.vm.volumns(rows);
		}.bind(this);

		request({ method: 'GET', url: '/image' }).then(this.images, this.error);
	},
	view: function(ctrl) {
		return layout(
			<div>
				<h2>Create Data Container</h2>
				<form onsubmit={ctrl.handleSubmit}>
					{ctrl.formErrors()}
					<div class="form-group">
						<label>Name</label>
						<input type="text" class="form-control" value={ctrl.vm.name()} onchange={m.withAttr('value', ctrl.vm.name)} />
					</div>
					<div class="form-group">
						<label>Base Image</label>
						<select class="form-control" value={ctrl.vm.image()} onchange={m.withAttr('value', ctrl.vm.image)}>
							<option value=""></option>
							{ctrl.images().map(function(item) {
								return <option value={item}>{item}</option>;
							})}
						</select>
					</div>
					<div class="form-group">
						<label>Volumns</label>
						<div class="row">
							<div class="col-md-8">
								<textarea rows="10" class="form-control" onchange={ctrl.handleVolumnsTextChange}>{ctrl.volumnsText()}</textarea>
							</div>
							<div class="col-md-4">
								<ul class="list-group">
									{ctrl.vm.volumns().map(function(item) {
										return <li class="list-group-item">{item}</li>;
									})}
								</ul>
							</div>
						</div>
					</div>
					<button type="submit" class="btn btn-primary">Create</button>
				</form>
			</div>
		);
	}
};