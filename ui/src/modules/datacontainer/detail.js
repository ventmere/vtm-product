/** @jsx m */
var m = require('mithril');
var _ = require('lodash');
var toastr = require('toastr');

var layout = require('../../layout');
var submodule = require('../../lib/submodule');
var filters = require('../../lib/filters');

var model = require('./model');

module.exports = {
	controller: function() {
		var id = m.route.param('id');
		this.error = m.prop(null);
		this.model = m.prop(null);
		model.load(id).then(this.model, this.error);
		this.handleRemove = function() {
			if (!confirm('Are you sure?')) {
				return;
			}
			model.destroy(id).then(function() {
				toastr.success('Data container deleted.');
				m.route('/datacontainer');
			}, this.error);

		}.bind(this);
	},
	view: function(ctrl) {
		var model = ctrl.model();
		if (model) {
			var deleteLink = '/datacontainer/' + model.id + '/delete';
			return layout(
				<div>
					<h2>Data Container: {model.name}</h2>
					<p>
						<button class="btn btn-danger" onclick={ctrl.handleRemove}>Delete</button>
					</p>
					<table class="table table-striped">
						<tbody>
							<tr>
								<th>ID</th>
								<td>{model.id}</td>
							</tr>
							<tr>
								<th>Created At</th>
								<td>{filters.date(model.created_at)}</td>
							</tr>
							<tr>
								<th>Image</th>
								<td>{model.inspect.Config.Image}</td>
							</tr>
							<tr>
								<th>Volumns</th>
								<td>{JSON.stringify(model.inspect)}</td>
							</tr>
						</tbody>
					</table>
				</div>
			);
		} else {
			var error = ctrl.error();
			return layout(
				<div class="alert alert-danger">
					{error ? error.message : 'Unknown error'} &nbsp;
					<button class="btn btn-default btn-sm" onclick={ctrl.handleRemove}>Remove this record</button>
				</div>
			);
		}
	}
};