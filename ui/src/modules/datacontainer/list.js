/** @jsx m */
var m = require('mithril');
var _ = require('lodash');

var layout = require('../../layout');
var submodule = require('../../lib/submodule');
var filters = require('../../lib/filters');

var model = require('./model');

module.exports = {
	controller: function() {
		this.list = model.loadAll();
	},
	view: function(ctrl) {
		var items = ctrl.list();
		var list;
		if (items.length) {
			list = items.map(function(item) {
				var link = '/instance/' + item.id;
				return (
					<tr>
						<td>
							<a href={link} config={m.route}>{item.name}</a>
						</td>
						<td>{item.id}</td>
						<td>{item.status.size}</td>
						<td>{filters.date(item.created_at)}</td>
					</tr>
				);
			});
		} else {
			list = (
				<tr>
					<td colspan="5" class="text-muted">
						No record.
					</td>
				</tr>
			);
		}

		return layout(
			<div>
				<a class="btn btn-primary" href="/datacontainer-create" config={m.route}>Create Data Container</a>
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>Name</th>
							<th>Container ID</th>
							<th>Size</th>
							<th>Created At</th>
						</tr>
					</thead>
					<tbody>{list}</tbody>
				</table>
			</div>
		);	
	}
};