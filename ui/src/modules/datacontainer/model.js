var m = require('mithril');
var _ = require('lodash');
var request = require('../../lib/request');
var model = {
	create: function(data) {
		return m.request({
			method: 'POST',
			url: '/datacontainer',
			data: data
		});
	},

	loadAll: function() {
		return request({
			method: 'GET',
			url: '/datacontainer'
		});
	},

	load: function(id) {
		return request({
			method: 'GET',
			url: '/datacontainer/' + id
		});
	},

	destroy: function(id) {
		if (!id) {
			throw new Error('Invalid id');
		}
		return request({
			method: 'DELETE',
			url: '/datacontainer/' + id
		});
	}
};

module.exports = model;