/** @jsx m */
var m = require('mithril');
var _ = require('lodash');
var toastr = require('toastr');

var layout = require('../../layout');
var submodule = require('../../lib/submodule');
var filters = require('../../lib/filters');

var model = require('./model');

module.exports = {
	controller: function() {
		var id = m.route.param('id');
		this.error = m.prop(null);
		this.instance = m.prop(null);
		model.load(id).then(this.instance, this.error);
		this.handleRemove = function() {
			model.destroy(id).then(function() {
				toastr.success('Instance deleted.');
				m.route('/instance');
			}, this.error);

		}.bind(this);
	},
	view: function(ctrl) {
		var instance = ctrl.instance();
		if (instance) {
			var deleteLink = '/instance/' + instance.id + '/delete';
			return layout(
				<div>
					<h2>Instance: {instance.name}</h2>
					<table class="table table-striped">
						<tbody>
							<tr>
								<th>Created At</th>
								<td>{filters.date(instance.created_at)}</td>
							</tr>
							<tr>
								<th>Updated At</th>
								<td>{filters.date(instance.updated_at)}</td>
							</tr>
						</tbody>
					</table>
				</div>
			);
		} else {
			var error = ctrl.error();
			return layout(
				<div class="alert alert-danger">
					{error ? error.message : 'Unknown error'} &nbsp;
					<button class="btn btn-default btn-sm" onclick={ctrl.handleRemove}>Remove this record</button>
				</div>
			);
		}
	}
};