/** @jsx m */
var m = require('mithril');
var _ = require('lodash');

var layout = require('../../layout');
var submodule = require('../../lib/submodule');
var filters = require('../../lib/filters');

var model = require('./model');

module.exports = {
	controller: function() {
		var statusMap = m.prop({});
		this.getStatus = function(id) {
			var map = statusMap();
			if (map[id] !== undefined) {
				return <span>{map[id] || 'Stopped'}</span>
			} else {
				return <div class="loader"></div>;
			}
		};

		this.list = model.loadAll().then(function(data) {
			var idList = _.pluck(data, 'id');
			if (idList.length) {
				m.request({
					url: '/instance-status-batch',
					method: 'POST',
					data: idList
				}).then(statusMap);
			}
			return data;
		});
	},
	view: function(ctrl) {
		var items = ctrl.list();
		var list;
		if (items.length) {
			list = items.map(function(item) {
				var link = '/instance/' + item.id;
				return (
					<tr>
						<td>
							<a href={link} config={m.route}>{item.name}</a>
						</td>
						<td>{ctrl.getStatus(item.id)}</td>
						<td>{item.id}</td>
						<td>{filters.date(item.created_at)}</td>
						<td>{filters.date(item.updated_at)}</td>
					</tr>
				);
			});
		} else {
			list = (
				<tr>
					<td colspan="5" class="text-muted">
						No record.
					</td>
				</tr>
			);
		}

		return layout(
			<div>
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>Name</th>
							<th>Status</th>
							<th>Instance ID</th>
							<th>Created At</th>
							<th>Updated At</th>
						</tr>
					</thead>
					<tbody>{list}</tbody>
				</table>
			</div>
		);	
	}
};