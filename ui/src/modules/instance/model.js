var m = require('mithril');
var _ = require('lodash');
var request = require('../../lib/request');

var model = {
	create: function(options) {
		options = options || {};
		var data = {
			service_id: options.service_id,
			name: options.name,
			image: options.image,
			env: options.env
		};
		return m.request({
			method: 'POST',
			url: '/instance',
			data: data
		});
	},

	loadAll: function() {
		return request({
			method: 'GET',
			url: '/instance'
		});
	},

	load: function(id) {
		return request({
			method: 'GET',
			url: '/instance/' + id
		});
	},

	destroy: function(id) {
		if (!id) {
			throw new Error('Invalid id');
		}
		return request({
			method: 'DELETE',
			url: '/instance/' + id
		});
	}
};

module.exports = model;