/** @jsx m */
var m = require('mithril');
var _ = require('lodash');

var layout = require('../../layout');
var submodule = require('../../lib/submodule');
var filters = require('../../lib/filters');
var dispatcher = require('../../dispatcher');

var model = require('./model');
var form = require('./form');

module.exports = {
	controller: function() {
		this.service = model.forge();
		this.error = m.prop(null);
		var props = {
			model: this.service,
			error: this.error,
			onSubmit: function() {
				this.error(null);
				model.save(this.service).then(function(data) {
					dispatcher.emit('successMessage', 'Created.');
					m.route('/service/' + data.id);
				}, this.error);
			}.bind(this)
		};
		this.form = submodule(form, props);
	},
	view: function(ctrl) {
		return layout(
			<div>
				<h2>Create Service</h2>
				{ctrl.form()}
			</div>
		);
	}
};