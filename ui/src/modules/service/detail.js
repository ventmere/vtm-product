/** @jsx m */
var m = require('mithril');
var _ = require('lodash');

var layout = require('../../layout');
var submodule = require('../../lib/submodule');
var filters = require('../../lib/filters');

var model = require('./model');

module.exports = {
	controller: function() {
		this.service = model.load(m.route.param('id'));
	},
	view: function(ctrl) {
		var service = ctrl.service();
		if (service) {
			var editLink = '/service/' + service.id + '/update';
			var deleteLink = '/service/' + service.id + '/delete';
			var instantiateLink = '/service/' + service.id + '/instantiate';
			return layout(
				<div>
					<h2>Service: {service.name}</h2>
					<p>
						<a class="btn btn-primary" href={instantiateLink} config={m.route}>Create Instance</a>&nbsp;
						<a class="btn btn-default" href={editLink} config={m.route}>Edit</a>
					</p>
					<table class="table table-striped">
						<tbody>
							<tr>
								<th>Created At</th>
								<td>{filters.date(service.created_at)}</td>
							</tr>
							<tr>
								<th>Updated At</th>
								<td>{filters.date(service.updated_at)}</td>
							</tr>
							<tr>
								<th>Images</th>
								<td>
									{service.images.map(function(item) {
										return <div class="label label-info">{item}</div>; 	
									})}
								</td>
							</tr>
							<tr>
								<th>ENV</th>
								<td>
									<ul class="list-unstyled">
										{service.env.map(function(env) {
											return (
												<li>
													<span style="display:inline-block;width: 150px">{env.name}</span>
													<span>{env.value}</span>
												</li>
											);
										})}
									</ul>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			);
		} else {
			return layout(
				<div class="alert alert-danger">Service not found</div>
			);
		}
	}
};