/** @jsx m */
var m = require('mithril');
var _ = require('lodash');

var submodule = require('../../lib/submodule');
var filters = require('../../lib/filters');
var request = require('../../lib/request');
var FormErrors = require('../../components/form-error');
var PropEditor = require('../../components/propeditor');
var Modal = require('../../components/modal');

var model = require('./model');

var LinkImageModal = {
	controller: function(prop) {
		this.allImages = request({ method: 'GET', url: '/image' });
		this.handleSelectionChange = function(image, value) {
			var propValue = prop();
			var pos = propValue.indexOf(image);
			if (value) {
				if (pos === -1) {
					propValue.push(image);
				}
			} else {
				if (pos !== -1) {
					propValue.splice(pos, 1);
				}
			}
			prop(propValue);
		};

		this.isChecked = function(image) {
			return prop().indexOf(image) !== -1;	
		};
	},
	view: function(ctrl) {
		return (
			<div>
				<h2>Select Images</h2>
				<p>
					<ul class="list-unstyled">
						{ctrl.allImages().map(function(item) {
							return (
								<li>
									<label>
										<input type="checkbox" 
											checked={ctrl.isChecked(item)} 
											onchange={m.withAttr('checked', ctrl.handleSelectionChange.bind(null, item))} />
										{item}
									</label>
								</li>);
						})}
					</ul>
				</p>
			</div>
		);
	}
};

var ServiceImages = {
	controller: function(prop) {
		this.prop = prop;
		this.modal = submodule(Modal);

		this.addImage = function() {
			Modal.module(LinkImageModal, prop);
		};

		this.removeImage = function(index) {
			prop().splice(index, 1);	
		};
	},
	view: function(ctrl) {
		return (
			<div>
				<p>
					<button type="button" class="btn btn-default" onclick={ctrl.addImage}>Add Image</button>
				</p>
				<ul class="list-group">
					{ctrl.prop().map(function(item, index) {
						return (
							<li class="list-group-item">
								<label>{item}</label>
								<p>
									<button type="button" class="btn btn-danger btn-xs" onclick={ctrl.removeImage.bind(null, index)}>Unlink</button>
								</p>
							</li>
						);
					})}
				</ul>
				{ctrl.modal({class: "m-modal-animation-3"})}
			</div>
		);
	}
};

module.exports = {
	controller: function(props) {
		this.props = props;
		this.serviceImages = submodule(ServiceImages, props.model.images);
		this.formErrors = submodule(FormErrors, props.error);
		this.envEditor = submodule(PropEditor, { model: props.model.env });
		this.handleSubmit = function(e) {
			e.preventDefault();
			props.onSubmit();
		};
	},
	view: function(ctrl) {
		return (
			<form onsubmit={ctrl.handleSubmit.bind(ctrl)}>
				{ctrl.formErrors()}
				<div class="form-group">
					<label>Name</label>
					<input type="text" class="form-control" value={ctrl.props.model.name()} onchange={m.withAttr('value', ctrl.props.model.name)} />
				</div>
				<div class="form-group">
					<label>Images</label>
					{ctrl.serviceImages()}
				</div>
				<div class="form-group">
					<label>Environmental Variables</label>
					{ctrl.envEditor()}
				</div>
				<button type="submit" class="btn btn-primary">Save</button>
			</form>
		);
	}
};