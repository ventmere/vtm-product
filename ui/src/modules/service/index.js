exports.list = require('./list');
exports.create = require('./create');
exports.update = require('./update');
exports.detail = require('./detail');
exports.instantiate = require('./instantiate');
