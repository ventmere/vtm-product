/** @jsx m */
var m = require('mithril');
var _ = require('lodash');
var toastr = require('toastr');

var layout = require('../../layout');
var submodule = require('../../lib/submodule');
var filters = require('../../lib/filters');
var PropEditor = require('../../components/propeditor');
var FormErrors = require('../../components/form-error');

var model = require('./model');
var instanceModel = require('../instance/model');

var vm = {
	serviceId: m.prop(null),
	name: m.prop(''),
	image: m.prop(''),
	env: m.prop([]),

	init: function() {
		this.serviceId(null);
		this.name('');
		this.image('');
		this.env([]);
		return this;
	}
};

module.exports = {
	controller: function() {
		this.vm = vm.init();
		this.service = model.load(m.route.param('id')).then(function(data) {
			this.vm.serviceId(data.id);
			this.vm.env(data.env);
			return data;
		}.bind(this));

		this.error = m.prop(null);

		this.formErrors = submodule(FormErrors, this.error);
		this.envEditor = submodule(PropEditor, { model: vm.env, fixedKeys: true });

		this.handleSubmit = function(e) {
			e.preventDefault();
			this.error(null);
			instanceModel.create({
				service_id: this.vm.serviceId(),
				name: this.vm.name(),
				image: this.vm.image(),
				env: this.vm.env()
			}).then(function(data) {
				toastr.success('Instance created');
				m.route('/instance/' + data.id);
			}.bind(this), this.error);
		}.bind(this);
	},
	view: function(ctrl) {
		var service = ctrl.service();
		if (service) {
			var editLink = '/service/' + service.id + '/update';
			var deleteLink = '/service/' + service.id + '/delete';
			var instantiateLink = '/service/' + service.id + '/instantiate';
			return layout(
				<div>
					<h2>Create Service Instance: {service.name}</h2>
					<form onsubmit={ctrl.handleSubmit}>
						{ctrl.formErrors()}
						<div class="form-group">
							<label>Instance Name</label>
							<input type="text" class="form-control" value={ctrl.vm.name()} onchange={m.withAttr('value', ctrl.vm.name)} />
						</div>
						<div class="form-group">
							<label>Image</label>
							<select class="form-control" value={ctrl.vm.image()} onchange={m.withAttr('value', ctrl.vm.image)}>
								<option value=""></option>
								{service.images.map(function(tag) {
									return <option value={tag}>{tag}</option>
								})}
							</select>
						</div>
						<div class="form-group">
							<label>Environmental Variables</label>
							{ctrl.envEditor()}
						</div>
						<button class="btn btn-primary" type="submit">Create Instance</button>
					</form>
				</div>
			);
		} else {
			return layout(
				<div class="alert alert-danger">Service not found</div>
			);
		}
	}
};