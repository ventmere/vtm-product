/** @jsx m */
var m = require('mithril');
var _ = require('lodash');
var toastr = require('toastr');

var layout = require('../../layout');
var submodule = require('../../lib/submodule');
var filters = require('../../lib/filters');
var dispatcher = require('../../dispatcher');

var model = require('./model');
var form = require('./form');

module.exports = {
	controller: function() {
		var id = m.route.param('id');
		this.service = null;
		this.error = m.prop(null);

		this.handleDelete = function() {
			if (!confirm('Are you sure?')) {
				return;
			}
			model.destroy(this.service.id()).then(function() {
				m.route('/service');
				toastr.success('Deleted.');
			});
		};

		model.load(id).then(function(data) {
			this.service = model.forge(data);
			var props = {
				model: this.service,
				error: this.error,
				onSubmit: function() {
					this.error(null);
					model.save(this.service).then(function(data) {
						dispatcher.emit('successMessage', 'Updated.');
					}, this.error);
				}.bind(this)
			};
			this.form = submodule(form, props);
		}.bind(this));
	},
	view: function(ctrl) {
		return ctrl.service ? layout(
			<div>
				<h2 class="page-header">Update Service</h2>
				<p>
					<a class="btn btn-danger" onclick={ctrl.handleDelete.bind(ctrl)}>Delete</a>&nbsp;
				</p>
				{ctrl.form()}
			</div>
		) : layout(
			<div class="alert alert-danger">Service not found</div>
		);
	}
};