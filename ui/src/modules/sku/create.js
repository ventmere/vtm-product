/** @jsx m */
var m = require('mithril');
var _ = require('lodash');
var toastr = require('toastr');

var layout = require('../../layout');
var submodule = require('../../lib/submodule');
var filters = require('../../lib/filters');
var request = require('../../lib/request');
var FormErrors = require('../../components/form-error');

var model = require('./model');
var SKUForm = require('./form');

var getViewModel = function() {
	return {
		id: m.prop(''),
		name: m.prop(''),
		description: m.prop(''),
		seo_title: m.prop(''),
		seo_description: m.prop(''),
		track_inventory: m.prop(false),
		inventory: m.prop(null),

		options: m.prop([]),
		images: m.prop([]),
		collections: m.prop([]),
		tags: m.prop([]),
		price: {
			price: m.prop(0.0),
			currency: m.prop('CAD')
		},
		meta: {
			price_compare_at: m.prop(null),
			weight: m.prop(null),
			barcode: m.prop('')
		}
	};
};

module.exports = {
	controller: function() {
		this.error = m.prop(null);
		this.images = m.prop([]);
		this.volumnsText = m.prop('');
		this.vm = getViewModel();
		this.formErrors = submodule(FormErrors, this.error);
		this.form = submodule(SKUForm, {
			model: this.vm,
			onSubmit: function(e) {
				e.preventDefault();
				this.error(null);
				model.create(this.vm).then(function(data) {
					toastr.success('Data container created.');
					m.route('/datacontainer/' + data.id);
				}, this.error);
			}.bind(this)
		});
	},
	view: function(ctrl) {
		return layout(
			<div>
				<h2>Create SKU</h2>
				<code>{JSON.stringify(ctrl.vm)}</code>
				{ctrl.formErrors()}
				{ctrl.form()}
			</div>
		);
	}
};