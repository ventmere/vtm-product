/** @jsx m */
var m = require('mithril');
module.exports = {
	controller: function(props) {
		this.props = props;
		this.vm = props.model;
	},
	view: function(ctrl) {
		return (
			<form onsubmit={ctrl.props.onSubmit}>
				<fieldset>
					<legend>Product Details</legend>
					<div class="form-group">
						<label>SKU</label>
						<input type="text" class="form-control" value={ctrl.vm.id()} onchange={m.withAttr('value', ctrl.vm.id)} />
					</div>
					<div class="form-group">
						<label>Name</label>
						<input type="text" class="form-control" value={ctrl.vm.name()} onchange={m.withAttr('value', ctrl.vm.name)} />
					</div>
					<div class="form-group">
						<label>Description</label>
						<textarea rows="10" class="form-control" value={ctrl.vm.description()} onchange={m.withAttr('value', ctrl.vm.description)}></textarea>
					</div>
				</fieldset>
				<fieldset>
					<legend>Inventory &amp; variants</legend>
					<div class="form-group">
						<label>Price</label>
						<input type="text" class="form-control" value={ctrl.vm.price.price()} onchange={m.withAttr('value', ctrl.vm.price.price)} />
					</div>
					<div class="form-group">
						<label>Compare at price</label>
						<input type="text" class="form-control" value={ctrl.vm.meta.price_compare_at()} onchange={m.withAttr('value', ctrl.vm.meta.price_compare_at)} />
					</div>
					<div class="form-group">
						<label>Weight</label>
						<input type="text" class="form-control" value={ctrl.vm.meta.weight()} onchange={m.withAttr('value', ctrl.vm.meta.weight)} />
					</div>
					<div class="form-group">
						<label>Barcode</label>
						<input type="text" class="form-control" value={ctrl.vm.meta.barcode()} onchange={m.withAttr('value', ctrl.vm.meta.barcode)} />
					</div>
					<div class="checkbox">
						<label>
							<input type="checkbox" value={ctrl.vm.track_inventory()} onchange={m.withAttr('checked', ctrl.vm.track_inventory)} /> Track Inventory
						</label>
					</div>
					<div class="form-group" style={{ display: ctrl.vm.track_inventory() ? 'block' : 'none'}}>
						<label>Quantity</label>
						<input type="text" class="form-control" value={ctrl.vm.inventory()} onchange={m.withAttr('value', ctrl.vm.inventory)} />
					</div>
					<div class="form-group">
						<label>Options</label>
					</div>
				</fieldset>
				<fieldset>
					<legend>Images</legend>

				</fieldset>
				<fieldset>
					<legend>Collections</legend>
					
				</fieldset>
				<fieldset>
					<legend>Tags</legend>
					
				</fieldset>
				<fieldset>
					<legend>Search Engines</legend>
					
				</fieldset>
				<fieldset>
					<legend>Visibility</legend>
					
				</fieldset>
				<button type="submit" class="btn btn-primary">Create</button>
			</form>
		);
	}
};