/** @jsx m */
var m = require('mithril');
var _ = require('lodash');

var layout = require('../../layout');
var submodule = require('../../lib/submodule');
var filters = require('../../lib/filters');

var model = require('./model');

module.exports = {
	controller: function() {
		this.list = model.loadAll();
	},
	view: function(ctrl) {
		var items = ctrl.list();
		var list;
		if (items.length) {
			list = items.map(function(item) {
				var link = '/sku/' + item.id;
				return (
					<tr>
						<td>
							<a href={link} config={m.route}>{item.name}</a>
						</td>
						<td>{filters.date(item.created_at)}</td>
						<td>{filters.date(item.updated_at)}</td>
					</tr>
				);
			});
		} else {
			list = (
				<tr>
					<td colspan="4" class="text-muted">
						No record.
					</td>
				</tr>
			);
		}

		return layout(
			<div>
				<p>
					<a href="/shoppingcart/sku-create" config={m.route} class="btn btn-primary">Create SKU</a>
				</p>
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>Name</th>
							<th>Created At</th>
							<th>Updated At</th>
						</tr>
					</thead>
					<tbody>{list}</tbody>
				</table>
			</div>
		);	
	}
};