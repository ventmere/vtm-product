var m = require('mithril');
var _ = require('lodash');
var request = require('../../lib/request');

var model = {
	forge: function(props) {
		var model = {
			id: m.prop(null),
			name: m.prop(''),
			images: m.prop([]),
			env: m.prop([])
		};
		if (props) {
			for (var key in props) {
				if (props.hasOwnProperty(key) && model.hasOwnProperty(key)) {
					model[key](props[key]);
				}
			}
		}
		return model;
	},

	loadAll: function() {
		return request({
			method: 'GET',
			url: '/sku'
		});
	},

	load: function(id) {
		return request({
			method: 'GET',
			url: '/sku/' + id
		});
	},

	save: function(model) {
		var id = model.id();
		if (!id) {
			return request({
				method: 'POST',
				url: '/sku',
				data: model
			});
		} else {
			return request({
				method: 'PUT',
				url: '/sku/' + id,
				data: model
			});
		}
	},

	destroy: function(id) {
		if (!id) {
			throw new Error('Invalid id');
		}
		return request({
			method: 'DELETE',
			url: '/sku/' + id
		});
	}
};

module.exports = model;